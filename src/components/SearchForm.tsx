import React, { useState, useEffect, useRef } from 'react'


interface Props {
  // get data from parent
  query: string,
  // get function to Call parent
  onSearch: (query: string) => void
}

const SearchForm = ({ query: queryFromParent, onSearch }: Props) => {
  const [query, setQuery] = useState(queryFromParent)
  const queryInputRef = useRef<HTMLInputElement>(null)

  const sendQueryToParent = () => {
    onSearch(query)
  }

  useEffect(() => {
    setQuery(queryFromParent)
  }, [queryFromParent])

  useEffect(() => {
    // queryInputRef.current && queryInputRef.current.focus()
    queryInputRef.current?.focus()
  }, [])

  return (
    <div className=" mb-3">
      <div className="input-group">

        <input type="text" className="form-control" placeholder="Search"
          value={query} ref={queryInputRef}
          onChange={e => setQuery(e.target.value)}
          onKeyDown={e => e.key === 'Enter' && sendQueryToParent()} />

        <div className="input-group-append">
          <button className="btn btn-outline-secondary"
            onClick={sendQueryToParent}>Search</button>

        </div>
      </div>
      {170 - query.length} / 170
    </div>
  )
}


export default SearchForm


  // const updateQuery = (event: React.ChangeEvent<HTMLInputElement>) => {
  //   setQuery(event.target.value)
  // }