// tsrafce
import React, { useEffect, useState } from 'react'
import SearchForm from '../components/SearchForm'
import SearchResults from '../components/SearchResults'
import { Album } from '../model/Search'
import { searchAlbums } from '../services/Search'
import { useLocation, useHistory } from 'react-router-dom'


const mockData: Album[] = [
  {
    id: '123', name: 'Test 123', images: [{ url: 'https://www.placecage.com/c/300/300', width: 300, height: 300 }]
  },
  {
    id: '234', name: 'Test 234', images: [{ url: 'https://www.placecage.com/c/300/300', width: 300, height: 300 }]
  },
  {
    id: '345', name: 'Test 345', images: [{ url: 'https://www.placecage.com/c/300/300', width: 300, height: 300 }]
  }, ,
  {
    id: '456', name: 'Test 456', images: [{ url: 'https://www.placecage.com/c/400/400', width: 300, height: 300 }]
  },
] as Album[]

const SearchView = (props: any) => {
  // const [a,b,c] = Array(1,2,3)
  const [isLoading, setIsLoading] = useState(false)
  const [results, setResults] = useState(mockData)
  const [query, setQuery] = useState('batman')
  const { search: searchQuery } = useLocation()
  const { push, replace } = useHistory()

  useEffect(() => {
    const query = new URLSearchParams(searchQuery).get('q') || 'batman';
    
    setQuery(query);
    setIsLoading(true)
    searchAlbums(query)
      .then(resp => {
        setResults(resp)
        setIsLoading(false)
      })
  }, [searchQuery])

  const search = (query: string) => {
    push({
      search: '?q=' + query
    })
  }


  return (
    <div>
      {/* .row*2>.col */}
      <div className="row">
        <div className="col">
          <SearchForm query={query} onSearch={search} />
        </div>
      </div>

      <div className="row">
        <div className="col">
          {isLoading && <p>Please wait, loading...</p>}

          <SearchResults results={results} />
        </div>
      </div>

    </div>
  )
}

export default SearchView
