import React, { useEffect, useState, useRef } from 'react'
import { useParams } from 'react-router-dom'
import { getAlbumById } from '../services/Search'
import { Album, Track } from '../model/Search'
import AlbumCard from '../components/AlbumCard'

interface Props {

}

const AlbumDetails = (props: Props) => {
  const { album_id } = useParams()
  const [album, setAlbum] = useState<Album | null>(null)
  const [currentTrack, setCurrentTrack] = useState<Track>()
  const audioRef = useRef<HTMLAudioElement>(null)

  useEffect(() => {
    getAlbumById(album_id).then(resp => setAlbum(resp))
  }, [album_id])

  useEffect(() => {
    if (!audioRef.current || !currentTrack) { return }

    audioRef.current.src = currentTrack?.preview_url!;
    audioRef.current.volume = 0.2;
    audioRef.current.play()

  }, [currentTrack])

  if (!album) { return <p>Loading...</p> }

  return (
    <div>
      <div className="row">
        <div className="col">
          <AlbumCard album={album} />
        </div>
        <div className="col">
          <audio ref={audioRef} controls={true} className="w-100" />

          <div className="list-group">
            {album.tracks?.items.map(track =>
              <div onClick={() => setCurrentTrack(track)}
                key={track.id}
                className={
                  `list-group-item ${
                  track.id === currentTrack?.id ? 'active' : ''
                  }`}>
                {track.name}
              </div>)}
          </div>

        </div>
      </div>

    </div>
  )
}

export default AlbumDetails
