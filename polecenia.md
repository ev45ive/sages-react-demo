# Nowy projekt
npx create-react-app --template typescript demo-react

<!-- lub: -->
# Zoom
Mikrofon : Alt + A

# Instalacje 
https://nodejs.org/en/
https://git-scm.com/download/win
https://code.visualstudio.com/

# Pobieramy projekt z git
Start > Uruchom > cmd.exe
cd C:\WaszUlubionyKatalog\

git clone https://bitbucket.org/ev45ive/sages-react-demo.git
cd sages-react-demo
npm install
npm run start

# Bootstrap CSS
npm i bootstrap
<!-- lub -->
yarn add bootstrap

# VS Code 
https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets


# Ankieta
https://tiny.pl/7f6hr - ankieta

# Chrome React Devtools
https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi
